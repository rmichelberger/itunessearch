## Notes ##

1. I've selected swift as the programming language, becuase I think it can be easily used for a small prototype,
    however in production code there is a high risk of rewriting the code when a new version of the language is coming out. (Swift 3.0 will have some bigger changes for example)

2. I've used the Master/Detail sample project provided by Xcode
    
3. I've created a develop branch, keeping the master branch for the release versions. There are several options for branch strategy.

- I have changed the end point form HTTP (as it was defined in the requirements) to HTTPS, so I did not have to add an exception for iOS 9 ATS to enable HTTP communication.

## Possible improvements ##

- multi language support (localized storyboards, language parameter in the search request)
- paging for the search result (on scroll load more item)
- add continuous integration
- setup fastlane for the project (automatic screenshot)
- more design (color theme, app icon, ...)
- some fancy animation (tap on the circle artwork animates the image fullscreen)
- add a preview button (a play button inside the artwork image)
- it would be nice to have bigger resolution of the artwork provided by the backend
- add more error case unit test and more UI test
- add more search feature: filters provided by the search API (country, attribute, ...)
- setup git flow
- add Watch app

## License ##

```

Copyright 2016 Roland Michelberger

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```